import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as const
from scipy import integrate

class TTF:
  def __init__(self, f0, filename):
    """ class for the computation of TTF (odd Ez)

    """
    self.U_CST = 1.

    self.f0 = f0 # temporary number
    self.omega0 = 2.*np.pi*self.f0
    self.lambda0 = const.c/self.f0
 
    self.lst_Ez = []
    self.lst_z = []

    # load E field file
    iFile = open(filename, 'r')
    # ignore initial two lines
    iFile.readline()
    iFile.readline()
  
    lst_z_tmp = []
    for line in iFile:
      split_line = line.split()
      if not split_line:
        print("line is empty")
        continue
      z = float(split_line[0])/1e3
      Ez = float(split_line[1])/1e6
      lst_z_tmp.append(z)
      self.lst_Ez.append(Ez)

    ## prepare good orientation for the following analysis
    index_max = self.lst_Ez.index(max(self.lst_Ez))
    index_min = self.lst_Ez.index(min(self.lst_Ez))

    if index_max < index_min:
      self.lst_Ez = - np.array(self.lst_Ez)

    ## shift center of the cavity to 0
    z_center = (lst_z_tmp[index_max] + lst_z_tmp[index_min])/2.
    self.lst_z = np.array(lst_z_tmp)-z_center


  def GetEz(self):
    return (self.lst_Ez, self.lst_z)

  def GetVoltage(self):
    integrand = np.absolute(np.array(self.lst_Ez))
    int_Ez=integrate.cumtrapz(integrand, self.lst_z)[-1]
    return int_Ez

  def GetTTF(self, n_beta, min_beta, max_beta):

    lst_beta = []
    lst_TTF = []

    int_Ez = self.GetVoltage()

    for i_beta in range(0, n_beta):
      beta = min_beta + (max_beta-min_beta)*float(i_beta)/ float(n_beta)

      integrand = np.array(self.lst_Ez) * np.sin(2.*np.pi/self.lambda0 * (np.array(self.lst_z))/beta)
      W_over_q=integrate.cumtrapz(integrand, self.lst_z)[-1]
      TTF = W_over_q/int_Ez

      lst_beta.append(beta)
      lst_TTF.append(TTF)

    return (lst_TTF, lst_beta)

  def GetR_over_Q(self, n_beta, min_beta, max_beta):

    lst_beta = []
    lst_TTF = []
    lst_R_over_Q = []

    int_Ez = self.GetVoltage()

    for i_beta in range(0, n_beta):
      beta = min_beta + (max_beta-min_beta)*float(i_beta)/ float(n_beta)

      integrand = np.array(self.lst_Ez) * np.sin(2.*np.pi/self.lambda0 * (np.array(self.lst_z))/beta)
      W_over_q=integrate.cumtrapz(integrand, self.lst_z)[-1]
      TTF = W_over_q/int_Ez

      lst_beta.append(beta)
      lst_TTF.append(TTF)

    lst_R_over_Q = np.power(np.array(lst_TTF) * int_Ez*1e6, 2)/(self.omega0 * self.U_CST)
    return (lst_R_over_Q, lst_beta)
