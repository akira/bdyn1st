       Curve Length / mm                run 4/real
----------------------------------------------------------------------
                       0                       2968.2832
              0.99703979                       3815.9438
               1.9940643                       4274.9595
               2.9911041                       5029.6953
               3.9881287                       5873.3916
               4.9851685                       6806.0737
                5.982193                       7827.7124
               6.9792328                        9172.124
               7.9762573                       11163.028
               8.9732971                       15855.065
               9.9703217                       21352.361
               10.967361                       27655.082
               11.964386                       34763.043
               12.961426                       42165.445
                13.95845                       47760.066
                14.95549                       69467.844
               15.952515                       100876.72
               16.949554                       132317.77
               17.946594                       161502.83
               18.943619                       186011.56
               19.940659                       211662.38
               20.937683                       252379.19
               21.934723                       329185.53
               22.931747                        430138.5
               23.928787                       532103.06
               24.925812                       638318.38
               25.922852                       749928.06
               26.919876                       865009.13
               27.916916                       1012940.1
                28.91394                       1165867.8
                29.91098                       1283730.4
                30.90802                       1395698.4
               31.905045                       1500764.1
               32.902069                       1597682.4
               33.899109                         1689707
               34.896149                       1775453.8
               35.893173                       1847256.9
               36.890213                       1891562.1
               37.887238                       1934049.8
               38.884277                       1971274.5
               39.881302                       2006841.8
               40.878342                       2040752.5
               41.875366                         2071824
               42.872406                       2101235.3
               43.869438                       2129606.5
                44.86647                         2158121
               45.863503                       2183117.8
               46.860535                       2207907.3
               47.857567                       2232536.3
               48.854599                       2257005.3
               49.851631                       2281313.5
               50.848663                       2305461.8
               51.845695                       2329449.8
               52.842728                       2353277.8
               53.839767                       2376945.5
               54.836792                       2400839.3
               55.833832                       2425302.3
               56.830856                         2449999
               57.827896                       2474929.3
               58.824921                       2501538.8
                59.82196                       2530416.5
               60.818993                       2559396.8
               61.816025                       2588161.8
               62.813057                       2617255.3
               63.810089                       2645943.8
               64.807121                       2674931.5
               65.804153                       2704258.3
               66.801186                       2733924.5
               67.798218                       2763276.5
                68.79525                         2791734
               69.792282                       2820732.8
               70.789314                       2854397.5
               71.786346                       2888893.8
               72.783386                         2924312
               73.780418                       2960526.5
               74.777451                       2997444.5
               75.774483                         3035066
               76.771515                         3074128
               77.768547                       3115794.5
               78.765579                       3158141.3
               79.762611                       3201166.3
               80.759644                         3244871
               81.756676                         3289255
               82.753708                       3334317.8
                83.75074                         3380060
               84.747772                         3426482
               85.744804                       3473582.5
               86.741837                       3521361.8
               87.738876                       3567473.3
               88.735909                         3612767
               89.732941                       3658394.8
               90.729973                       3704356.5
               91.727005                         3750652
               92.724037                       3801301.5
               93.721069                         3853155
               94.718102                       3913596.8
               95.715134                       3974549.5
               96.712166                         4039323
               97.709198                       4096420.5
                98.70623                         4153556
               99.703262                       4211481.5
               100.70029                       4270051.5
               101.69733                         4325614
               102.69436                       4381205.5
               103.69139                         4440210
               104.68843                       4501727.5
               105.68546                         4561695
                106.6825                       4620871.5
               107.67953                       4679257.5
               108.67656                       4736851.5
               109.67359                         4792724
               110.67062                       4841260.5
               111.66766                       4886252.5
               112.66469                         4907446
               113.66172                         4890186
               114.65875                         4855653
               115.65578                       4803843.5
               116.65282                         4743453
               117.64985                         4673304
               118.64688                         4484803
               119.64392                       4280535.5
               120.64095                       4040744.5
               121.63799                         3777266
               122.63502                       3484747.5
               123.63205                       3136104.3
               124.62908                       2744643.5
               125.62611                       2333686.5
               126.62315                       1983390.4
               127.62018                       1679107.9
               128.61722                         1383677
               129.61424                       1115252.9
               130.61128                       873827.06
               131.60831                       698633.75
               132.60535                       585327.19
               133.60237                       487569.16
               134.59941                       393731.13
               135.59644                       303816.25
               136.59348                       222709.38
                137.5905                       157424.48
               138.58754                       134931.95
               139.58456                       114296.42
                140.5816                       95517.227
               141.57863                       78595.031
               142.57567                       63529.234
               143.57269                       47857.672
               144.56973                       31735.203
               145.56677                       25646.475
                146.5638                        22396.57
               147.56084                       19360.318
               148.55786                       16537.813
                149.5549                       13928.967
               150.55193                       11605.879
               151.54897                       9271.4814
               152.54599                       7197.1948
               153.54303                         5405.02
               154.54005                       3892.0999
               155.53709                       2658.1902
               156.53412                       1767.5067
               157.53116                       1318.2427
               158.52818                       958.40698
               159.52522                       657.13397
               160.52226                       422.65836
               161.51929                       254.80469
               162.51633                       127.66433
               163.51335                       15.819666
               164.51039                      -49.213249
               165.50742                      -7.4870362
               166.50446                       36.096973
               167.50148                       38.721531
               168.49852                       26.600113
               169.49554                       34.250229
               170.49258                        23.69956
               171.48961                      -3.7982795
               172.48665                      -38.225128
               173.48367                      -122.79108
               174.48071                       -228.9057
               175.47775                      -519.04114
               176.47478                      -868.17426
               177.47182                      -1235.0322
               178.46884                      -1676.4449
               179.46588                      -2186.4824
               180.46291                      -2814.5925
               181.45995                       -3525.019
               182.45697                      -4317.7402
               183.45401                      -5192.7783
               184.45103                      -6150.1094
               185.44807                      -7189.7622
                186.4451                      -10158.135
               187.44214                      -14609.929
               188.43916                      -19086.879
                189.4362                      -23753.586
               190.43323                      -28907.398
               191.43027                      -34007.551
               192.42731                      -39053.973
               193.42433                      -54736.957
               194.42137                       -75170.43
                195.4184                      -96447.086
               196.41544                      -119583.09
               197.41246                      -145512.78
                198.4095                      -181690.11
               199.40652                      -225122.56
               200.40356                       -265665.5
               201.40059                      -367408.16
               202.39763                      -485252.66
               203.39465                      -610048.81
               204.39169                      -743706.75
               205.38872                      -889123.25
               206.38576                      -1120637.5
               207.38278                      -1313402.1
               208.37982                      -1646942.3
               209.37686                      -2029223.3
               210.37389                      -2382475.3
               211.37093                      -2761895.5
               212.36795                      -3137659.8
               213.36499                      -3493854.5
               214.36201                      -3816810.5
               215.35905                      -4066462.8
               216.35608                      -4299776.5
               217.35312                        -4525184
               218.35014                        -4730954
               219.34718                      -4802251.5
               220.34421                        -4833314
               221.34125                      -4852353.5
               222.33827                        -4863045
               223.33531                        -4865605
               224.33235                        -4859922
               225.32938                      -4845021.5
               226.32642                      -4801400.5
               227.32344                        -4748037
               228.32048                        -4694050
                229.3175                      -4638466.5
               230.31454                        -4582414
               231.31157                      -4518743.5
               232.30861                        -4453521
               233.30563                        -4387914
               234.30267                        -4324891
                235.2997                        -4263721
               236.29674                      -4206646.5
               237.29376                        -4148882
                238.2908                      -4090004.5
               239.28784                      -4030403.5
               240.28487                      -3971633.8
               241.28191                      -3916017.3
               242.27893                      -3860603.8
               243.27597                        -3805392
               244.27299                        -3750384
               245.27003                      -3699881.5
               246.26706                      -3650180.3
                247.2641                      -3600529.5
               248.26112                      -3550929.5
               249.25816                      -3501287.5
               250.25519                      -3451139.3
               251.25223                      -3404351.3
               252.24925                        -3361402
               253.24629                      -3319314.3
               254.24332                      -3278089.5
               255.24036                      -3240163.5
                256.2374                      -3202919.3
               257.23444                        -3165883
               258.23145                      -3129054.8
               259.22849                      -3092433.3
               260.22552                      -3056019.8
               261.22256                      -3019813.5
               262.21957                      -2983816.5
               263.21661                      -2947144.8
               264.21365                        -2909493
               265.21069                        -2872234
                266.2077                      -2835368.5
               267.20474                      -2798894.3
               268.20178                        -2762812
               269.19882                      -2727122.8
               270.19583                        -2691827
               271.19287                      -2661467.3
               272.18991                      -2637778.3
               273.18695                      -2610564.5
               274.18399                        -2582713
                 275.181                        -2555465
               276.17804                      -2528819.5
               277.17508                      -2502845.5
               278.17212                        -2477442
               279.16913                      -2452250.8
               280.16617                      -2427269.8
               281.16321                      -2402499.3
               282.16025                        -2377940
               283.15726                      -2353592.3
                284.1543                      -2329509.5
               285.15134                        -2305456
               286.14838                      -2281403.3
               287.14539                        -2256924
               288.14243                      -2232184.3
               289.13947                        -2207558
               290.13651                      -2182824.8
               291.13354                        -2157787
               292.13055                        -2132741
               293.12759                        -2107144
               294.12463                      -2076092.6
               295.12167                      -2042345.3
               296.11868                      -2007125.8
               297.11572                      -1970430.5
               298.11276                        -1933051
                299.1098                      -1881466.1
               300.10681                      -1816846.3
               301.10385                      -1748937.9
               302.10089                      -1677742.9
               303.09793                      -1603261.9
               304.09497                      -1515710.5
               305.09198                      -1403380.9
               306.08902                        -1279361
               307.08606                      -1152097.9
                308.0831                      -1019983.1
               309.08011                      -883723.38
               310.07715                         -753221
               311.07419                      -630012.13
               312.07123                      -514096.69
               313.06824                      -423368.53
               314.06528                      -333497.88
               315.06232                      -273192.94
               316.05936                      -232642.72
               317.05637                      -193939.05
               318.05341                      -157075.31
               319.05045                      -122052.69
               320.04749                      -88408.203
               321.04453                      -57996.938
               322.04153                      -48650.742
               323.03857                      -40490.484
               324.03561                      -33399.551
               325.03265                      -26968.625
               326.02966                      -22299.324
                327.0267                      -16802.951
               328.02374                      -12294.388
               329.02078                      -10287.216
               330.01779                      -9156.5811
               331.01483                      -8221.2676
               332.01187                       -6607.165
               333.00891                       -5010.916
               334.00592                      -4307.8701
               335.00296                      -3989.7913
                     336                      -3871.4873