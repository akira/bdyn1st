import bdyn1st as akira
import numpy as np
import matplotlib.pyplot as plt

i_figure = 0;

test = akira.TTF(101.28e6, 'beam0.txt')

############## check loaded E-field #################
lst_Ez, lst_z = test.GetEz()

plt.figure(num=i_figure, figsize=(10, 8))
plt.rc('text', usetex=True)
plt.plot(lst_z, lst_Ez, 'k-')
plt.xlabel('z [m]')
plt.ylabel(r"Ez [MV/m]")
plt.grid(True)
i_figure += 1


############## check TTF(beta) #################
min_beta = 0.01
max_beta = 0.99
n_beta = 1000

lst_TTF, lst_beta = test.GetTTF(n_beta, min_beta, max_beta)

plt.figure(num=i_figure, figsize=(10, 8))
plt.rc('text', usetex=True)
plt.plot(lst_beta, lst_TTF, 'k-')
plt.xlabel(r'\beta')
plt.ylabel(r'TTF')
plt.grid(True)
i_figure += 1

############## check R/Q(beta) #################
min_beta = 0.01
max_beta = 0.99
n_beta = 1000

lst_R_over_Q, lst_beta = test.GetR_over_Q(n_beta, min_beta, max_beta)

plt.figure(num=i_figure, figsize=(10, 8))
plt.rc('text', usetex=True)
plt.plot(lst_beta, lst_R_over_Q, 'k-')
plt.xlabel(r'\beta')
plt.ylabel(r'R/Q')
plt.grid(True)
i_figure += 1

plt.show()
